/*
    ¡No modificar!
    Script para añadir acciones cada vez que se desplaza el usuario entre slides.
    Útil para mostrar modals, animar elementos, cambiar niveles de progreso, etc.
    Para añadir funcionalidad en cada slide al ingresar a éste, usar el método onEnter en el script individual de cada uno.
*/

const presentation = require('presentation');
const swal = require('sweetalert2');
const _ = require('lodash');
const defaultGroupSlider = require('defaultGroupSlider.js');

module.exports = {
    delegateSlideEvents: function(){
        this.switchToSlide = presentation.switchToSlide;
        presentation.switchToSlide = function(args){
            var $currentSlide = presentation.currentProps.$slide;
            var slideId = $currentSlide.attr('data-slide');
            var mod = undefined;
            try {
                mod = require("slide" + slideId + ".js");
            } catch (error) {
                //Not all slides have a script
                //throw('Looks like slide' + v + '.js doesn\'t exist. Can\'t require it.');
            }

            if(_.isObject(mod) && typeof mod.onSlide === 'function'){
                var handlerAction = mod.onSlide(args);
                if(handlerAction === true){
                    return module.exports.switchToSlide(args);
                }
            }

            else{
                if(defaultGroupSlider.slide(_.merge(
                    args,
                    {
                        $slide: $('.slide[data-slide="' + slideId + '"]')
                    }
                )) === true){
                    return module.exports.switchToSlide(args);
                }
            }
        }
    },
    addHooks: function () {
        var self = this;
        module.exports.delegateSlideEvents();
        $("#presentation").on("slide", function (e, i) {
            $("body").removeClass("cursor-pointer");
            var slideId = presentation.currentProps.$slide.attr("data-slide");
            var mod = undefined;
            try {
                mod = require("slide" + slideId + ".js");
            } catch (error) {
                //Not all slides have a script
            }

            if(_.isObject(mod) && typeof mod.onEnter === 'function'){
                mod.onEnter();
            }
        });

        
    }
}

