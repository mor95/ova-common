/*
  Script para finalizar OVA y desplazarse al primer slide, habilitando
  el PDF de descarga.
*/

const _ = require('lodash');
const swal = require('sweetalert2');
const stageResize = require('stage-resize');
const presentation = require('presentation');

module.exports = function(args){
        if (args.finishedOva === true) {
          swal({
            target: stageResize.currentProps.$stage.get(0),
            customClass: "ova-themed",
            showCloseButton: true,
            showConfirmButton: false,
            type: "success",
            html: 'some markup' 
          }).then(function (e) {
            presentation.switchToSlide({
              slide: $('.slide[data-slide="2"]')
            });
            var $pdf = $(".pdf").removeClass("disabled"),
              $a = $pdf.find("a");
            $a.attr("href", $a.attr("_href"));
            $pdf.find("i").addClass("animated flip infinite")
          })
        }
   
}
